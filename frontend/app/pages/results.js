// // // // //
// IMPORTS
// // // // //

// react
import { useEffect, useState } from 'react';

// react bootstrap
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';

// next
import { useRouter } from 'next/router';
import Head from 'next/head';

// custom components
import TzNavbar from '../components/Navbar.js';
import TzFooter from '../components/Footer.js';
import LoadingSpinner from '../components/LoadingSpinner.js';

// font awesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck, faTimes } from '@fortawesome/free-solid-svg-icons';

// css
import componentStyles from '../styles/ReviewQuestion.module.css';
import pageStyles from '../styles/results.module.css'

// app context
import { useAppContext, getSessionStatus } from '../context/main.js';

const appContext = {
    context: null,
    setContext: null
};


// // // // //
// PAGE
// // // // //
export default function ResultsPage() {
    
    // get context
    [appContext.context, appContext.setContext] = useAppContext();
    
    // router for redirections
    const router = useRouter();
    
    // show loading spinner until session is know
    const [loadingSpinner, setLoadingSpinner] = useState(true);
    
    // logged in status
    const [isUserLoggedIn, setIsUserLoggedIn] = useState(false);
    
    // change loading spinner after loading
    // user can visit this page if not logged if on trial
    // otherwise, should be redirected to index
    useEffect(async () => {
        
        const sessionStatus = await getSessionStatus(appContext.context, appContext.setContext);
        
        if (!sessionStatus) {
            if (!appContext.context.trial) {
                router.push('/');
            }
        }
        
        setLoadingSpinner(false);
        setIsUserLoggedIn(sessionStatus);
        
    }, []);
    
    // render page
    return (
      
        <div className="flexGrowPage">
        
            <Head>
                <title>TZ - Results</title>
            </Head>
              
            <TzNavbar />
        
            {loadingSpinner && <LoadingSpinner />}
            
            {
            
                !loadingSpinner &&
            
                <div className="d-flex flexGrowDiv lineHeight justify-content-center">
                    <Results router={router} setLoadingSpinner={setLoadingSpinner} />
                </div>
                
            }
            
            {!isUserLoggedIn && !appContext.context.trial && <LoadingSpinner />}
            
            <TzFooter />
            
        </div>
      
    );
    
}


// // // // //
// HELPER COMPONENTS
// // // // //

// if user is trying the app
// show results and suggest register
function Results({ router, setLoadingSpinner }) {
    
    // display user's performance
    const [score, setScore] = useState('');
    
    // display questions
    const [reviewQuestions, setReviewQuestions] = useState(false);
    
    // get user score
    useEffect(() => {
        setScore(getScore());
    });
    
    // handle user action
    function handleClick(event) {
        
        if (event.target.value === 'reg') {
            setLoadingSpinner(true)
            router.push('/register');
        } else if (event.target.value === 'review') {
            setReviewQuestions(true);
        } else if (event.target.value === 'hide') {
            setReviewQuestions(false);
        } else if (event.target.value === 'play') {
            router.push('/actions/play')
        } else {
            return false;
        }
        
        // always return something
        return true;
        
    }
    
    // render component
    return (
        
        <div className={pageStyles.margin}>
            <h5 className={`${pageStyles.margin}`}>{score}</h5>
            <Button className={`${pageStyles.margin} shadow-none actionButton ripple`} onClick={handleClick} value={reviewQuestions ? "hide" : "review"}>{reviewQuestions ? "Hide Review" : "Review Questions"}</Button>
            {
                appContext.context.trial ?
                <Button className={`${pageStyles.margin} shadow-none actionButton ripple`} onClick={handleClick} value="reg">Register</Button> :
                <Button className={`${pageStyles.margin} shadow-none actionButton ripple`} onClick={handleClick} value="play">Play Again</Button> 
            }
            {
                reviewQuestions && 
                Object.keys(appContext.context.score).map(key => <ReviewQuestion key={`reviewQuestion${key}`} question={key} /> )
            }
        </div>    
        
    );
    
}

// let a user review their own answers
function ReviewQuestion({ question }) {
    
    // display score with style
    const success = <FontAwesomeIcon icon={faCheck} />;
    const fail = <FontAwesomeIcon icon={faTimes} />;
    
    // set radios / checkboxes
    const optionType = appContext.context.questions[question]['multipleChoice'] ? "checkbox" : "radio";
    
    // display color to indicate which questions were correct
    const titleStyle = appContext.context.score[question] ? componentStyles.iconCorrect : componentStyles.iconIncorrect;
    
    // render component
    return (
        
        <div className={componentStyles.margin}>
            <Card className={componentStyles.padding}>
                <Card.Title className={titleStyle}>{appContext.context.score[question] ? success : fail} Question {question}</Card.Title>
                <Card.Text>{appContext.context.questions[question]['question']}</Card.Text>
                {
                    appContext.context.questions[question]['answers'].map((answer, index) => {
                    
                        const labelStyle = appContext.context.questions[question]['correctAnswers'][index] ? componentStyles.answerCorrect : componentStyles.answerIncorrect;
                        
                        return (
                            <Form.Check className={labelStyle} key={`answer${index}`} label={answer} type={optionType} checked={appContext.context.answers[question][index]} />
                        );
                    })
                }
                <Card.Title className={componentStyles.explanationTitle}>Explanation:</Card.Title>
                <Card.Text>{appContext.context.questions[question]['explanation']}</Card.Text>
            </Card>
        </div>    
        
    );
    
}


// // // // //
// HELPER FUNCTIONS
// // // // //

// process user score
// returns a string that will be displayed in a component
function getScore() {
    
    // count correct answers
    // count total answers
    let score = 0;
    const keys = Object.keys(appContext.context.score);
    const totalAnswers = keys.length;
    keys.forEach(key => {
        if (appContext.context.score[key] === true) {
            score++;
        }
    });
    
    // return string
    return `Score: ${(score / totalAnswers) * 100}% (${score}/${totalAnswers})`;
    
}