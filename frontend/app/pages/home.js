// // // // //
// IMPORTS
// // // // //

// react
import { useState, useEffect } from 'react';

// react-bootstrap components
import Spinner from 'react-bootstrap/Spinner';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row'
import Table from 'react-bootstrap/Table';
import Card from 'react-bootstrap/Card';

// next components
import Head from 'next/head';
import Link from 'next/link';

// next
import { useRouter } from 'next/router';

// custom components
import TzNavbar from '../components/Navbar.js';
import TzFooter from '../components/Footer.js';
import LoadingSpinner from '../components/LoadingSpinner.js';

// css
import styles from '../styles/home.module.css';
import Emoji from 'a11y-react-emoji';

// app context
import { useAppContext, getSessionStatus, getUsername } from '../context/main.js';

const appContext = {
    context: null,
    setContext: null
}

// // // // //
// PAGE
// // // // //
export default function Home() {
    
    // get context
    [appContext.context, appContext.setContext] = useAppContext();
    
    // router to handle redirects
    const router = useRouter();
    
    // show a loading spinner while page is checking if user is logged in
    const [loadingPage, setLoadingPage] = useState(true);
    
    // first check if user is logged in and redirect if not
    useEffect(async () => {
        
        if (await getSessionStatus(appContext.context, appContext.setContext)) {
            setLoadingPage(false);
        } else {
            router.push('login');
        }
        
    }, []);
    
    
    // render page
    return(
        
        <div className="flexGrowPage">
                
            <Head>
                <title>TZ - Home</title>
            </Head>
            
            <TzNavbar />
        
            { 
            
                loadingPage
            
                ? <LoadingSpinner /> 
                
                : <HomeBody />
                
            }
            
            <TzFooter />
        
        </div>    
        
    );
    
}


// // // // //
// CUSTOM COMPONENTS
// // // // //

// home body content
function HomeBody() {
    
    // render component
    return (
        
        <div className="flexGrowDiv">
        
            <div className="row no-gutters">
            
                <div className="col-sm">
                    <HomeCard card="userStats" />
                </div>
                
                <div className="col-sm">
                    <HomeCard card="generalStats" />
                </div>
                
            </div>
            
            <div className="row no-gutters">
            
                <div className="col-sm">
                    <HomeCard card="newQuestions" />
                </div>
                
                <div className="col-sm">
                    <HomeCard card="newForumQuestions" />
                </div>
                
            </div>
        
        </div>
        
    );
    
}


// // // // //
// CUSTOM COMPONENTS
// // // // //

// a component to display a card with info on the home page
function HomeCard({ card }) {
    
    // set card title
    const [title, setTitle] = useState('');
    
    // set card text
    const [text, setText] = useState('');
    
    // show a spinner for each individual card
    const [loadingSpinner, setLoadingSpinner] = useState(true);
    
    // set card content
    useEffect(async () => {
        
        // title
        setTitle(getTitle(card));
        setText(await getText(card));
        
        // hide loading spinner
        setLoadingSpinner(false);
        
    }, []);
    
    // render component
    return (
        
      <div className={`${styles.card} lineHeight`}>
      
          <Card className="text-center">
              <Card.Header className={styles.cardTitle}>{title}</Card.Header>
              {loadingSpinner ? <LoadingSpinner /> : <Card.Body>{text}</Card.Body>}
          </Card>
        
      </div>  
        
    );
    
}


// // // // //
// HELPER FUNCTIONS
// // // // //

// set cards text
function getTitle(card) {
    
    if (card === 'userStats') {
        return (<p>User Stats <Emoji symbol="🧗‍♀" label="user stats"/></p>);
    } else if (card === 'generalStats') {
        return (<p>General Stats <Emoji symbol="🤸" label="general stats"/></p>);
    } else if (card === 'newQuestions') {
        return (<p>Last 10 Questions Added <Emoji symbol="🥳" label="last ten questions"/></p>);
    } else if (card === 'newForumQuestions') {
        return (<p>Last 10 Forum Questions <Emoji symbol="🤯" label="last ten forum questions"/></p>);
    } else {
        return false;
    }
    
}

// a function to get user stats from the server
async function getText(card) {
    
    // get text from the server
    const response = await fetch(`/api/home?card=${card}&username=${appContext.context.username}`);
    const data = await response.json();
    
    // check if there was no error from the backend
    if (data.status !== 200) {
        return (
            <div>
                <p>{data.status}</p>
                <p>{data.message}</p>
            </div>
        );
    }
    
    // format text according to card type
    if (card === 'userStats') {
        return (
        <div>
            <p>Number of Answered Questions: {data.questionsRetrieved}</p>
            <p>Number of Correct Questions: {data.correctQuestions}</p>
            <p>Success Ratio: {data.successRatio}%</p>
        </div>
        );
    } else if (card === 'generalStats') {
        return (
            <div>
                <p>Total Number of Registered Users: {data.totalUsers}</p>
                <p>Total Number of Submitted Game Questions: {data.totalQuestions}</p>
            </div>    
        );
    } else if (card === 'newQuestions') {
        return (
            <div>
                {
                    data['questions'].map((question, index) => {
                        return(
                            <p key={`question${index}`}>{question.text}</p>
                        );
                    })
                }
            </div>
        );
    }
    
    // return text if an error occured
    return "error";
    
}