// // // // //
// IMPORTS
// // // // //


// // // // //
// API CALL
// // // // //

// fetch data from dynamodb using django
export default async function handler(request, response) {
    
    // django endpoint as env variables
    const djangoIp = process.env.DJANGO_MIDDLEMAN_IP;
    const djangoPort = process.env.DJANGO_MIDDLEMAN_PORT;
    
    // request sample questions
    const serverResponse = await fetch(`http://${djangoIp}:${djangoPort}/try`);
    const serverData = await serverResponse.json();
    
    // return backend response
    // status is always 200, as the backend always send a response
    // this is done so no errors appear in the console
    return response.status(200).json(serverData);
    
}