// // // // //
// IMPORTS
// // // // //

// nodejs runtime
import { writeFile } from 'fs';


// // // // //
// API CALL
// // // // //

// retrieve user info from server
export default async function handler(request, response) {
    
    // django endpoint as env variables
    const djangoIp = process.env.DJANGO_AUTH_IP;
    const djangoPort = process.env.DJANGO_AUTH_PORT;
    
    // check if token is valid
    const serverResponse = await fetch(`http://${djangoIp}:${djangoPort}/u?u=${request.query.user}`);
    const serverData = await serverResponse.json();
    
    // convert base64 string into buffer
    // console.log(atob(serverData['userInfo']['photo']['data']))
    // const buffer = Buffer.from(serverData['userInfo']['photo']['data'], 'base64');
    // writeFile('/tmp/test.png', buffer, (error) => {
    //     if(error) {
    //         console.log(error)
    //     }
    // });
    
    
    // return backend response
    // status is always 200, as the backend always send a response
    // however, getting user info may have not been successful, so the true response is inside the next api response
    // this is done so that no errors are displayed in the browser console
    return response.status(200).json(serverData);
    
}
