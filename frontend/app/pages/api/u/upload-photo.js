// // // // //
// IMPORTS and EXPORTS
// // // // //

// nodejs runtime
import { unlink, createReadStream } from 'fs';

// handle multipart uploads
import formidable from 'formidable';

// turn off custom parser
export const config = {
  api: {
    bodyParser: false,
  },
}


// // // // //
// API CALL
// // // // //

// upload photo to s3 and update dynamo
export default async function handler(request, response) {
    
    // parse incoming file
    const form = formidable({ multiples: true });
    
    form.parse(request, async (err, fields, files) => {
        
        if(!err) {
            
            // django endpoint as env variables
            const djangoIp = process.env.DJANGO_AUTH_IP;
            const djangoPort = process.env.DJANGO_AUTH_PORT;
            
            // path to image
            const tmpPath = files['file']['path'];
            
            // read file from tmp folder
            const file = createReadStream(tmpPath);
            
            const buffers = [];
            let buffersLength = 0;
            file.on('data', (chunk) => {
                buffers.push(chunk);
                buffersLength += chunk.length;
            });
            
            // create a single bytes buffer and send data to django
            // uses a base64 encoded string
            file.on('end', async () => {
                
                const bufferString = Buffer.concat(buffers, buffersLength).toString('base64');
                
                const serverResponse = await fetch(`http://${djangoIp}:${djangoPort}/u/upload-photo`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        username: fields['username'],
                        type: fields['type'],
                        data: bufferString,
                    }),
                });
                
                const serverData = await serverResponse.json();
                
                // return backend response
                // status is always 200, as the backend always send a response
                // however, getting user info may have not been successful, so the true response is inside the next api response
                // this is done so that no errors are displayed in the browser console
                return response.status(200).json(serverData);  
                
            });
            
            // delete file when stream is finished or when then are errors
            file.on('close', () => {
                unlink(tmpPath, () => true);
            });
            
            file.on('error', () => {
                unlink(tmpPath, () => true);
            });
            
        } else {
            return response.status(200).json({
                status: 404,
                message: 'Oh no! Something is wrong with the file. Please try again, or use a different file.'
            });
        }
    });
    
}
