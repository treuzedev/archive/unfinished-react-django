// // // // //
// IMPORTS
// // // // //


// // // // //
// API CALL
// // // // //

// retrieve all coins list from django server
export default async function handler(request, response) {
    
    // respond according to given route
    if (request.query.action === 'check') {
        return checkToken(request, response);
    } else if (request.query.action === 'refresh') {
        return tokenRefresh(request, response);
    } else {
        return response.status(200).json({token: false});
    }
    
}


// // // // //
// HELPER FUNCTIONS
// // // // //

// retrieve coin info for home table
async function checkToken(request, response) {
    
    // django endpoint as env variables
    const djangoIp = process.env.DJANGO_AUTH_IP;
    const djangoPort = process.env.DJANGO_AUTH_PORT;
    
    // check if token is valid
    const serverResponse = await fetch(`http://${djangoIp}:${djangoPort}/token/check`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${request.body}`
        }
    });
    const serverData = await serverResponse.json();
    
    // return if token is valid (=== true)
    if (serverData.status === 200) {
        return response.status(200).json({token: true});
    } else {
        return response.status(200).json({token: false});
    }
    
}

// remove unwanted coins
async function tokenRefresh(request, response) {
    
    // django endpoint as env variables
    const djangoIp = process.env.DJANGO_AUTH_IP;
    const djangoPort = process.env.DJANGO_AUTH_PORT;
    
    // check if token is valid
    const serverResponse = await fetch(`http://${djangoIp}:${djangoPort}/token/refresh`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            'refresh': request.body.refresh
        })
    });
    const serverData = await serverResponse.json();
    
    // return if token is valid (=== true)
    if (serverData.access) {
        return response.status(200).json({
            token: serverData.access,
            refresh: serverData.refresh
        });
    } else {
        return response.status(200).json({token: false});
    }
    
}