// // // // //
// IMPORTS
// // // // //


// // // // //
// API CALL
// // // // //

// post a new question using the django backend
export default async function handler(request, response) {
    
    // django endpoint as env variables
    const djangoIp = process.env.DJANGO_MIDDLEMAN_IP;
    const djangoPort = process.env.DJANGO_MIDDLEMAN_PORT;
    
    // try to post a new question
    const serverResponse = await fetch(`http://${djangoIp}:${djangoPort}/new-question`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(request.body)
    });
    
    const serverData = await serverResponse.json();
    
    // return backend response
    // status is always 200, as the backend always send a response
    // however, posting a new question may have not been successful, so the true response is inside the next api response
    // this is done so that no errors are displayed in the browser console
    return response.status(200).json(serverData);
    
}