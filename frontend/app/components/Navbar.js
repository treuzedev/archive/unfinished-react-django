// // // // //
// IMPORTS
// // // // //

// next imports
import { useRouter } from 'next/router';

// react imports
import { useState, useEffect } from 'react';

// next components
import Link from 'next/link';
import Image from 'next/image';

// react bootstrap components
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import DropdownButton from 'react-bootstrap/DropdownButton';
import Dropdown from 'react-bootstrap/Dropdown';

// css
import styles from '../styles/Navbar.module.css';
import Emoji from 'a11y-react-emoji';

// other imports
import lowerCase from 'lodash/lowerCase';

// helper functions
import { isLoggedIn, parseToken } from '../js/helpers.js';

// app context
import { useAppContext } from '../context/main.js';

const appContext = {
    context: null,
    setContext: null
}


// // // // //
// COMPONENT
// // // // //
export default function TzNavbar({ setLoadingSpinner, currentPage }) {
    
    // get app context
    [appContext.context, appContext.setContext] = useAppContext();
    
    // show current page loading spinner if needed
    function handleClick(event) {
        if (currentPage !== lowerCase(event.target.innerHTML)) {
            setLoadingSpinner(true);
        }
    }
    
    // render component
    return (
        
        <div className={styles.navbar}>
        
            <Navbar expand="sm" className="navbar-dark">
                
                <Link href="/">
                    <Nav.Link onClick={handleClick} className={`${styles.brand} lineHeight brandRipple`} href="/">
                        TZ
                    </Nav.Link>
                </Link>
                
                <Navbar.Toggle className={styles.hamburguer} aria-controls="basic-navbar-nav" />
                
                {
                    appContext.context.isUserLoggedIn
                    ? <LoggedIn handleClick={handleClick} />
                    : <LoggedOut handleClick={handleClick} />
                }
            
            </Navbar>
            
        </div>
        
    );
    
}

// // // // //
// HELPER COMPONENTS
// // // // //

// show logged in options
function LoggedIn({ handleClick }) {
    
    // create router to redirect user
    const router = useRouter();
    
    // logout user by deleting token and resetting context
    // redirect to index
    function handleLogout() {
        window.localStorage.removeItem('treuzeRefreshToken');
        appContext.setContext.isUserLoggedIn(false);
        appContext.setContext.token('');
        router.push('/');
    }
    
    // render component
    return (
        
        <Navbar.Collapse>
            <Nav className={`container justify-content-end ${styles.noRightPadMar} ${styles.actionButtonMargin}`}>
                <DropdownButton className={`${styles.actionButtonBackground} ${styles.buttonDropdown} lineHeight shadow-none actionButton ripple`} title="Actions">
                    <Dropdown.Item className={`${styles.dropItem} ${styles.ripple}`} href="/actions/play">Play a round <Emoji symbol="🤔" label="play a round"/></Dropdown.Item>
                    <Dropdown.Item className={`${styles.dropItem} ${styles.ripple}`} href="/actions/new-game-question">Post a new question <Emoji symbol="🦉" label="ask a question"/></Dropdown.Item>
                    <Dropdown.Item className={`${styles.dropItem} ${styles.ripple}`} href="/actions/new-forum-question">Ask a question in the forums <Emoji symbol="🙋" label="ask a question"/></Dropdown.Item>
                </DropdownButton>
                <Nav.Link className={`${styles.whiteText} ${styles.navLinkXPadding} textRipple`} href="/home">Home</Nav.Link>
                <Nav.Link className={`${styles.whiteText} ${styles.navLinkXPadding} textRipple`} href={`/u/${appContext.context.username}`}>Profile</Nav.Link>
                <Nav.Link className={`${styles.whiteText} ${styles.navLinkXPadding} textRipple`} href="#" onClick={handleLogout}>Logout</Nav.Link>
            </Nav>
        </Navbar.Collapse>    
        
    );
    
}

// show logged out option
function LoggedOut({ handleClick }) {
    
    // render component
    return (
        
        <Navbar.Collapse>
            <Nav className={`container justify-content-end ${styles.noRightPadMar}`}>
                <Link href="/try">
                    <Nav.Link onClick={handleClick} className={`${styles.whiteText} ${styles.navLinkXPadding} textRipple`} href="/try">
                        Try
                    </Nav.Link>
                </Link>
                <Link href="/register">
                    <Nav.Link onClick={handleClick} className={`${styles.whiteText} ${styles.navLinkXPadding} textRipple`} href="/register">
                        Register
                    </Nav.Link>
                </Link>
                <Link href="/login">
                    <Nav.Link onClick={handleClick} className={`${styles.whiteText} ${styles.navLinkXPadding} textRipple`} href="/login">
                        Login
                    </Nav.Link>
                </Link>
            </Nav>
        </Navbar.Collapse>    
        
    );
    
}