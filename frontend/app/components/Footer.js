// // // // //
// IMPORTS
// // // // //

// next js
import Link from 'next/link';


// react bootstrap components
import Nav from 'react-bootstrap/Nav';

// css
import styles from '../styles/Footer.module.css';

// font awesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faDiscord, faGitlab } from '@fortawesome/free-brands-svg-icons';


// // // // //
// COMPONENT
// // // // //
export default function TzFooter() {
    
    // render component
    return (
        
        <div className={`text-center ${styles.footer}`}>
        
            <div className="row no-gutters">
                <div className="col d-inline-flex justify-content-end">
                    <div className={`${styles.icon} ${styles.discordLogo} ripple`}>
                        <FontAwesomeIcon icon={faDiscord} />
                    </div>
                </div>
                <div className="col d-inline-flex justify-content-start">
                    <div className={`${styles.icon} ${styles.gitlabLogo} ripple`}>
                        <FontAwesomeIcon icon={faGitlab} />
                    </div>
                </div>
            </div>
            
            <div className="row justify-content-center no-gutters">
                <div className="col-sm-2">
                    <Link href="/disclaimer.js">
                        <Nav.Link className={styles.links} href="/info">
                            Info
                        </Nav.Link>
                    </Link>
                </div>
                <div className="col-sm-2">
                    <Link href="/terms">
                        <Nav.Link className={styles.links} href="/terms">
                            Terms of Use
                        </Nav.Link>
                    </Link>
                </div>
                <div className="col-sm-2">
                    <Link href="/privacy">
                        <Nav.Link className={styles.links} href="/privacy">
                            Privacy Policy
                        </Nav.Link>
                    </Link>
                </div>
                <div className="col-sm-2">
                    <Link href="/info">
                        <Nav.Link className={styles.links} href="/disclaimer">
                            Disclaimer
                        </Nav.Link>
                    </Link>
                </div>
            </div>
            
            <div className="row no-gutters">
                <div className="col">
                    <p className={styles.trademark}>@ 2021 Treuze</p>
                </div>
            </div>
            
        </div>
        
    );
    
}

// // // // //
// HELPER COMPONENTS
// // // // //
