// // // // //
// IMPORTS
// // // // //

// react bootstrap
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';


// // // // //
// CUSTOM COMPONENT
// // // // //
export default function SubmitModal({ modalState, handleClose }) {
    
    // render component
    return(
        
        <div>
        
            <Modal className="text-center" show={modalState.modalShow} onHide={handleClose} backdrop="static" keyboard={false}>
                
                <Modal.Header closeButton>
                  <Modal.Title>{modalState.formType}</Modal.Title>
                </Modal.Header>
                
                <Modal.Body className="lineHeight">{modalState.status}</Modal.Body>
                <Modal.Body className="lineHeight">{modalState.message}</Modal.Body>
                
                <Modal.Footer className="d-flex justify-content-center">
                  <Button className={`shadow-none actionButton ripple`} value={modalState.buttonValue} onClick={handleClose}>{modalState.buttonText}</Button>
                </Modal.Footer>
                
            </Modal>
        
        </div>    
        
    );
    
}