// // // // //
// IMPORTS
// // // // //

// react
import { useState, useEffect } from 'react';

// react-bootstrap components
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

// font awesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';

// custom components
import LoadingSpinner from './LoadingSpinner.js';
import SubmitModal from './SubmitModal.js';

// css
import styles from '../styles/NewGameQuestion.module.css';

// font awesome
import { faTimes } from '@fortawesome/free-solid-svg-icons';

// helpers


// // // // //
// CUSTOM COMPONENT
// // // // //
export default function NewGameQuestion({ username }) {
    
    // show loading spinner while question is being submitted
    const [loadingPage, setLoadingPage] = useState(false);
    
    // vars to handle input
    const [text, setText] = useState('');
    const [category, setCategory] = useState('');
    const [answer, setAnswer] = useState('');
    const [explanation, setExplanation] = useState('');
    
    // var to save question's categories
    const [categories, setCategories] = useState([]);
    
    // var to save answers
    const [answers, setAnswers] = useState([]);
    
    // var to save correct answer
    const [correctAnswers, setCorrectAnswers] = useState({});
    
    // show / hide modal state informing user about his action
    const [modalState, setModalState] = useState({});
    
    // handle user input
    function handleChange(event) {
        
        // update state according to which field is being updated
        const inputId = event.target.id;
        const inputValue = event.target.value;
        
        if (inputId === 'text') {
            setText(inputValue);
        } else if (inputId === 'category') {
            setCategory(inputValue);
        } else if (inputId === 'answer') {
            setAnswer(inputValue);
        } else if (inputId === 'explanation') {
            setExplanation(inputValue);
        } else {
            return false;
        }
        
        // always return something
        return true;
        
    }
    
    // add categories / answers to list
    // update correct answers object accordingly
    function addToList(event) {
        
        if (event.target.value === 'category') {
            if (checkInput('category', category)) {
                categories.push(category);
                setCategories(categories);
                setCategory('');
            } else {
                return false;
            }
        } else if (event.target.value === 'answer') {
            if (checkInput('answer', answer)) {
                answers.push(answer);
                correctAnswers[(answers.length - 1)] = false;
                setAnswers(answers);
                setCorrectAnswers(correctAnswers);
                setAnswer('');
            } else {
                return false
            }
        }
        
        // always return something
        return true;
        
    }
    
    // handle form submission
    async function handleSubmit(event) {
        
        // ux
        setLoadingPage(true);
        
        // prevent default submit behaviour
        event.preventDefault();
        
        // post question if all fields are valid
        // advise user to try again if fields are not valid
        const validity = checkFormValidty(text, answers, categories, correctAnswers, explanation);
        if (validity.status) {
            
            // post question
            const posting = await postQuestion(text, answers, categories, correctAnswers, explanation, username);
            
            // warn user if the question was, or not, successfully posted
            if (posting.status === 200) {
                
                // warn user
                setModalState({
                    formType: 'Question Submission',
                    message: 'Question has been posted successfully!',
                    buttonText: 'Great!',
                    buttonValue: '',
                    status: '',
                    modalShow: true
                });
                
                // clear all fields since submission was successful
                setText('');
                setAnswers([]);
                setCategories([]);
                setExplanation('');
                
            } else {
                
                // warn user
                setModalState({
                    formType: 'Question Submission',
                    message: 'Oh no! Something went wrong.. Please try again.',
                    buttonText: 'Okay..',
                    buttonValue: '',
                    status: posting.message,
                    modalShow: true
                });
                
            } 
            
        } else {
                
            // warn user
            setModalState({
                formType: 'Question Submission',
                message: 'Oh no! Something went wrong.. Please try again.',
                buttonText: 'Okay..',
                buttonValue: '',
                status: validity.message,
                modalShow: true
            });
            
        }
        
        // ux
        setLoadingPage(false);
        
        // if form / posting question was not valid, then warn user
        
        // always return something
        return true;
        
    }
    
    // handle modal close
    function handleClose(event) {
        setModalState({
            modalShow: false,
            buttonText: '',
            message: '',
            formType: ''
        });
    }
    
    // render component
    return (
        
        <div>
        
            {
                loadingPage
                
                ? <LoadingSpinner />
                
                : <Form onSubmit={handleSubmit}>
            
                    <Form.Group className={styles.formGroup}>
                        <Form.Label className={styles.margin}><FontAwesomeIcon icon={faInfoCircle} /></Form.Label>
                        <Form.Label className={styles.margin}>New Game Question Text</Form.Label>
                        <Form.Control className={styles.margin} as="textarea" rows={5} id="text" onChange={handleChange} placeholder={'Edit as a Markdown (or not! If you prefer just plain text).\nClick on the information icon for help'} value={text} />
                    </Form.Group>
                    
                    <Form.Group className={styles.formGroup}>
                        <Form.Label className={styles.margin}><FontAwesomeIcon icon={faInfoCircle} /></Form.Label>
                        <Form.Label className={styles.margin}>Add Question's Answers</Form.Label>
                        <Form.Control className={styles.margin} id="answer" onChange={handleChange} placeholder="Answer" value={answer} />
                        <Button className={`${styles.margin} shadow-none actionButton ripple`} onClick={addToList} value="answer" variant="primary">Add</Button>
                        {answers.length > 0 && <ChooseCorrectAnswer answers={answers} setAnswers={setAnswers} correctAnswers={correctAnswers} setCorrectAnswers={setCorrectAnswers} />}
                    </Form.Group>
                    
                    <Form.Group className={styles.formGroup}>
                        <Form.Label className={styles.margin}><FontAwesomeIcon icon={faInfoCircle} /></Form.Label>
                        <Form.Label className={styles.margin}>Add Question's Categories</Form.Label>
                        <Form.Control className={styles.margin} id="category" onChange={handleChange} placeholder="Category" value={category} />
                        <Button className={`${styles.margin} shadow-none actionButton ripple`} onClick={addToList} value="category" variant="primary">Add</Button>
                        {categories.length > 0 && <DisplayCategories categories={categories} setCategories={setCategories} />}
                    </Form.Group>
                    
                    
                    <Form.Group className={styles.formGroup}>
                        <Form.Label className={styles.margin}><FontAwesomeIcon icon={faInfoCircle} /></Form.Label>
                        <Form.Label className={styles.margin}>Explanation</Form.Label>
                        <Form.Control className={styles.margin} as="textarea" rows={5} id="explanation" onChange={handleChange} placeholder={'Edit as a Markdown (or not! If you prefer just plain text).\nClick on the information icon for help'} value={explanation} />
                    </Form.Group>
                    
                    <Button className={`${styles.submitButtonmargin} shadow-none actionButton ripple`} type="submit" variant="primary">Submit</Button>
                
                </Form>
            
            }
                
            <SubmitModal modalState={modalState} handleClose={handleClose} />
            
        </div>
        
    );
    
}


// // // // //
// CUSTOM COMPONENTS
// // // // //

// a component to display added categories
function DisplayCategories({ categories, setCategories }) {
    
    // remove a categorie from the categories list
    function removeCategory(event) {
        const tmp = categories.filter((element, index) => parseInt(event.currentTarget.id) !== index);
        setCategories(tmp);
    }
    
    return (
        
        <div className={`${styles.margin} lineHeight`}>
            <p>Categories Added:</p>
            {categories.map((category, index) => {
                return (
                    <div key={`category${index}`} className={`${styles.flexCenter} ${styles.categoryLabel}`}>
                        <p>{category}</p>
                        <FontAwesomeIcon id={index} onClick={removeCategory} icon={faTimes} />
                    </div>
                );
            })}
        </div>    
        
    );
    
}

// a component to choose a correct answer from a list of answers
function ChooseCorrectAnswer({ answers, setAnswers, correctAnswers, setCorrectAnswers }) {
    
    // update parent state in the child
    function handleCheckboxChange(event) {
        
        // update state
        correctAnswers[event.target.name] = !correctAnswers[event.target.name];
        
        // set state in parent
        setCorrectAnswers(correctAnswers);
        
        // always return something
        return true;
        
    }
    
    // remove an answer from the answers list
    function removeAnswer(event) {
        const tmp = answers.filter((element, index) => parseInt(event.currentTarget.id) !== index);
        setAnswers(tmp);
    }
    
    // render component
    return (
        
        <div className={`${styles.margin} lineHeight`}>
            <Form.Group>
                <Form.Label>Choose this question's correct answer(s)</Form.Label>
                {answers.map((answer, index) => {
                    return (
                        <div key={`questionCheckbox${index}`} className={`${styles.flexCenter} ${styles.lineHeight} ${styles.correctAnswer}`}>
                            <Form.Check className={styles.flexCenter} type="checkbox" label={answer} name={index} onChange={handleCheckboxChange} defaultChecked={correctAnswers[index]} />
                            <FontAwesomeIcon id={index} onClick={removeAnswer} icon={faTimes} />
                        </div>
                    );
                })}
            </Form.Group>
        </div>    
        
    );
    
}


// // // // //
//  HELPER FUNCTIONS
// // // // //

// check if input is valid
// category needs to be only one word
// all need not to be empty
function checkInput(type, input) {
    if (type === 'category') {
        if (input.length > 0 && input.split("\s+").length === 1) {
            return true;
        } else {
            return false;
        }
    } else if (type === 'answer') {
        if (input.length > 0) {
            return true;
        } else {
            return false;
        }
    }
}

// check if form is valid
function checkFormValidty(text, answers, categories, correctAnswers, explanation) {
    
    // variables
    const validity = {
        status: true,
        message: ''
    };
    
    // question's text must not be empty
    if (text.length < 1) {
        validity.message = 'Question\'s text must not be empty.';
        validity.status = false
    }
    
    // check if there are at least two answers
    if (answers.length < 1 ) {
        validity.message = 'Please add at least two answers.';
        validity.status = false
    }
    
    // check if there is at least one category
    if (categories.length < 1) {
        validity.message = 'Please add at least one category.';
        validity.status = false
    }
    
    // question's answers explanation must not be empty
    if (explanation.length < 1) {
        validity.message = 'Question\'s explanation text must not be empty.';
        validity.status = false
    }
    
    // check if there is at least one correct answer
    let flag = false;
    const keys = Object.keys(correctAnswers);
    
    if (keys.length > 0 && answers.length > 1) {
        
        // first check if there is a correct answer
        // if there is, loop can break
        // if after the loop is finished there is no correct answer, return false to indicate bad submission
        for (let i = 0; i < keys.length; i++) {
            if (correctAnswers[keys[i]]) {
                flag = true;
                break
            }
        }
        
        if (!flag) {
            validity.message = 'Please choose at least one correct answer.';
            validity.status = false
        }
        
    } else {
        validity.message = 'Please choose at least one correct answer.';
        validity.status = false
    }
    
    // if all tests passed, form is valid
    return validity;
    
}

// post a new question
async function postQuestion(text, answers, categories, correctAnswers, explanation, username) {
    
    // format parameters to send
    const params = {
        username: username,
        text: text,
        answers: answers,
        categories: categories,
        correctAnswers: correctAnswers,
        explanation: explanation
    };
    
    // post data to next api
    const response = await fetch('/api/new-game-question', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(params)
    });
    
    const data = await response.json();
    
    // always return something
    return data;
    
}