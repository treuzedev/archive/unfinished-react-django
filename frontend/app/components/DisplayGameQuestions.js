// // // // //
// IMPORTS
// // // // //

// react
import { useState, useEffect } from 'react';

// react bootstrap
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Pagination from 'react-bootstrap/Pagination';

// next
import { useRouter } from 'next/router';

// custom components
import LoadingSpinner from './LoadingSpinner.js';

// css
import styles from '../styles/DisplayGameQuestions.module.css';

// app context
import { useAppContext } from '../context/main.js';

const appContext = {
    context: null,
    setContext: null
};


// // // // //
// COMPONENT
// // // // //
export default function DisplayGameQuestions({ questions, reviewMode, setLoadingSpinner, setValidQuestions }) {
    
    // get context
    [appContext.context, appContext.setContext] = useAppContext();
    
    // router for logged in redirection
    const router = useRouter();
    
    // loading spinner
    const [isLoading, setIsLoading] = useState(true);
    
    // question to be displayed
    const [currentQuestion, setCurrentQuestion] = useState(0);
    
    // if answer has multiple choices, use checkboxes, else, use radio
    const [optionType, setOptionType] = useState('');
    
    // disable button if at the first / last question
    const [disablePrev, setDisablePrev] = useState(true);
    const [disableNext, setDisableNext] = useState(false);
    
    // populate empty fields
    // prevent the reading of undefined properties
    // this is only done the first time the component mount
    const [isFirstTime, setIsFirstTime] = useState(true);
    
    // for every question, iterate over every answer
    // they all start as false
    // in the end, populate the first question's answers
    // use tmp vars so new state is readily available
    // finally, create state to be used by the component
    // every time the user tries to play a round, add those questions to the seen stats
    let tmpCurrentQuestionAnswer = {};
    const tmpUserAnswers = {};
    
    if (isFirstTime) {
        
        questions.forEach((question, index) => {
            
            question['answers'].forEach((answer, index) => {
                tmpCurrentQuestionAnswer[index] = false;
            });
            
            tmpUserAnswers[index] = tmpCurrentQuestionAnswer;
            
            tmpCurrentQuestionAnswer = {};
            
        });
        
    }
    
    // keep track of user's answers to the current displayed question
    const [currentQuestionAnswer, setCurrentQuestionAnswer] = useState(tmpUserAnswers[0]);
    
    // keep track of all user answers
    const [userAnswers, setUserAnswers] = useState(tmpUserAnswers);
    
    // set answer type everytime user changes the question
    useEffect(async () => {
        
        // prevent adding questions to dynamo more than once
        // needs to be here because of async
        if (isFirstTime) {
            
            // add seen questions to dynamo
            // if this went wrong, then allow user to try again
            const n = questions.length;
            if (await addTotalQuestions(n)) {
                setIsFirstTime(false);
            } else {
                setValidQuestions(false);
            }
            
        }
        
        // answer type
        if (questions[currentQuestion]['multipleChoice']) {
            setOptionType('checkbox');
        } else {
            setOptionType('radio');
        }
        
        // remove loading spinner
        setIsLoading(false);
        
        // always return something
        return true;
        
    }, [questions[currentQuestion]]);
    
    // keep track of user's answers
    // both to current question and all other questions
    function handleCheckClick(event) {
        
        // if dealing with radios, first change all keys to false, then change the answer the user chose
        // if dealing with checkboxes, simply reverse the current value
        if (optionType === 'radio') {
            const tmpCurrentQuestionAnswer = {};
            Object.keys(currentQuestionAnswer).forEach(key => {
                tmpCurrentQuestionAnswer[key] = false;
            });
            tmpCurrentQuestionAnswer[event.target.value] = true;
            setCurrentQuestionAnswer(tmpCurrentQuestionAnswer);
        } else if (optionType === 'checkbox') {
            setCurrentQuestionAnswer({
                ...currentQuestionAnswer,
                [event.target.value]: !currentQuestionAnswer[event.target.value]
            });
        }
        
    }
    
    // react when user clicks a button
    // move up or down the questions list
    // handle pagination movement
    // if in review mode, check if answer is correct
    // toggle movement buttons on/off
    function handleMovement(event) {
        
        // use new state in this function
        let newQuestion = currentQuestion;
        
        // always return something if input in not valid
        if (event.target.value === 'prev') {
            newQuestion--;
            setCurrentQuestion(newQuestion);
        } else if (event.target.value === 'next') {
            newQuestion++;
            setCurrentQuestion(newQuestion);
        } else if (event.target.value === 'check') {
            console.log('checkButton');
        } else if (event.target.getAttribute('value')) {
            newQuestion = parseInt(event.target.getAttribute('value'));
            setCurrentQuestion(newQuestion);
        } else {
            return false;
        }
        
        // update global answers
        setUserAnswers({
            ...userAnswers,
            [currentQuestion]: currentQuestionAnswer
        });
        
        // set current question previous answers
        setCurrentQuestionAnswer(userAnswers[newQuestion]);
        
        // buttons
        if (newQuestion === 0) {
            setDisablePrev(true);
            setDisableNext(false);
        } else if (newQuestion === (questions.length - 1)) {
            setDisablePrev(false);
            setDisableNext(true);
        } else {
            setDisablePrev(false);
            setDisableNext(false);
        }
        
        // always return something
        return true;
        
    }
    
    // handle questions submission
    function handleSubmit(event) {
        
        // prevent normal form submission
        event.preventDefault();
        
        // show loading spinner while questions are being submitted
        setLoadingSpinner(true);
        
        // show loading indication
        setIsLoading(true);
        
        // ensure current question is registered and new state can be used immediately
        const tmpUserAnswers = {
            ...userAnswers,
            [currentQuestion]: currentQuestionAnswer
        };
        
        setUserAnswers(tmpUserAnswers);
        
        // process questions
        processQuestions(questions, tmpUserAnswers);
        
        // move to results page
        router.push('/results');
        
        // TODO REMOVE THIS?
        setIsLoading(false);
        
    }
    
    // render component
    return (
        
        <div className="d-flex justify-content-center align-items-center flexGrowPage">
        
            {
                isLoading
                
                ? <LoadingSpinner />
                
                : <div className={`${styles.div}`}>
                
                    <Questination currentQuestion={currentQuestion} handleMovement={handleMovement} numberOfQuestions={(questions.length - 1)} />
                
                    <Card className={`${styles.card}`}>
                        
                        <Card.Text>{questions[currentQuestion]['question']}</Card.Text>
                        
                        <Form onSubmit={handleSubmit}>
                        
                            <fieldset>
                                <Form.Group>
                                    {
                                        questions[currentQuestion]['answers'].map((answer, index) => {
                                            return (
                                                <Form.Check id="test" className={styles.checked} key={`answer${index}`} label={answer} name="answers" onChange={handleCheckClick} type={optionType} value={index} checked={currentQuestionAnswer[index]} />
                                            );
                                        })
                                    }
                                </Form.Group>
                            </fieldset>
                            
                            <Button className={`${styles.button} actionButton shadow-none ripple`} disabled={disablePrev} onClick={handleMovement} value="prev">Previous</Button>
                            
                            <Button className={`${styles.button} actionButton shadow-none ripple`} disabled={disableNext} onClick={handleMovement} value="next">Next</Button>
                            
                            {reviewMode && <Button className={`${styles.button} actionButton shadow-none ripple`} onClick={handleMovement} value="check">Check</Button>}
                            
                            <Button className={`${styles.button} actionButton shadow-none ripple`} type="submit" variant="primary">Submit</Button>
                            
                        </Form>
                        
                    </Card>
                
                </div>
            }
            
        </div>    
        
    );
    
}


// // // // //
// HELPER COMPONENTS
// // // // //
function Questination({ currentQuestion, handleMovement, numberOfQuestions }) {
    
    // render component
    return(
        
        <Pagination>
        
            {currentQuestion >= 3 && <Pagination.Item onClick={handleMovement} value={0} >{0}</Pagination.Item>}    
            {currentQuestion > 3 && <Pagination.Ellipsis />}
            
            {currentQuestion >= 2 && <Pagination.Item onClick={handleMovement} value={currentQuestion - 2}>{currentQuestion - 2}</Pagination.Item>}
            {currentQuestion >= 1 && <Pagination.Item onClick={handleMovement} value={currentQuestion - 1}>{currentQuestion - 1}</Pagination.Item>}
            <Pagination.Item className={styles.customActive} active>{currentQuestion}</Pagination.Item>
            {currentQuestion <= (numberOfQuestions - 1) && <Pagination.Item onClick={handleMovement} value={currentQuestion + 1}>{currentQuestion + 1}</Pagination.Item>}
            {currentQuestion < (numberOfQuestions - 1) && <Pagination.Item onClick={handleMovement} value={currentQuestion + 2}>{currentQuestion + 2}</Pagination.Item>}
            
            {currentQuestion < (numberOfQuestions - 3) && <Pagination.Ellipsis />}
            {currentQuestion < (numberOfQuestions - 2) && <Pagination.Item onClick={handleMovement} value={numberOfQuestions}>{numberOfQuestions}</Pagination.Item>}
            
        </Pagination>    
        
    );
    
}


// // // // //
// HELPER FUNCTIONS
// // // // //

// a function to process user's choices
async function processQuestions(questions, answers) {
    
    // keep track of how many questions the user answered correctly
    const score = {};
    
    // compare each question correct answers to the user's answers
    questions.forEach((question, index1) => {
        
        let flag = true;
        
        Object.keys(question.correctAnswers).forEach((key, index2) => {
            
            if (answers[index1][index2] !== question.correctAnswers[index2]) {
                flag = false;
            }
            
        });
        
        score[index1] = flag;
        
    });
    
    // save context before moving on
    appContext.setContext.questions(questions);
    appContext.setContext.answers(answers);
    appContext.setContext.score(score);
    
    // TODO update user stats in dynamo
    await addCorrectQuestions(score);
    
    // always return something
    return true;
    
}

// a function number of questions being played to the user's stats via dynamodb
// allows a user reload if update fails
async function addTotalQuestions(n) {
    
    const response = await fetch(`/api/user-stats/add-total-questions`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            'n': n,
            'username': appContext.context.username,
        }),
    });
    
    const data = await response.json();
    
    if (data.status === 200) {
        return true;
    } else {
        return false;
    }
    
}

// a function number of questions being played to the user's stats via dynamodb
async function addCorrectQuestions(score) {
    
    const response = await fetch(`/api/user-stats/add-correct-questions`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            'score': score,
            'username': appContext.context.username,
        }),
    });
    
    const data = await response.json();
    
    if (data.status === 200) {
        return true;
    } else {
        return false;
    }
    
}