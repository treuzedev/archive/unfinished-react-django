// // // // //
// IMPORTS
// // // // //

// react-bootstrap components
import Spinner from 'react-bootstrap/Spinner';

// css
import styles from '../styles/LoadingSpinner.module.css';

// // // // //
// CUSTOM COMPONENT
// // // // //
export default function LoadingSpinner() {
    
    return (
        
        <div className="flexGrowDiv d-flex align-items-center justify-content-center">
        
            <div className={`row d-flex ${styles.margin}`}>
                <Spinner key="1" animation="grow" />
                <Spinner key="2" animation="grow" />
                <Spinner key="3" animation="grow" />
            </div>
        
        </div>    
        
    );
    
}