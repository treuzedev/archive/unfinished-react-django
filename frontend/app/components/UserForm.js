// // // // //
// IMPORTS
// // // // //

// next imports
import { useRouter } from 'next/router';

// react bootstrap
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

// react
import { useState, useEffect } from 'react';

// helpers
import { checkUsername, checkEmail, checkPassword } from '../js/helpers.js';

// custom components
import SubmitModal from './SubmitModal.js';
import LoadingSpinner from './LoadingSpinner.js';

// other imports
import capitalize from 'lodash/capitalize';

// css
import styles from '../styles/UserForm.module.css';

// app context
import { useAppContext } from '../context/main.js';

const appContext = {
    context: null,
    setContext: null
};


// // // // //
// COMPONENT
// // // // //
export default function UserForm({ formType }) {
    
    // get context
    [appContext.context, appContext.setContext] = useAppContext();
    
    // show loading spinner until session is know
    const [loadingSpinner, setLoadingSpinner] = useState(true);
    
    // allow redirects
    const router = useRouter();
    
    // var to check if form has correct input
    const [validated, setValidated] = useState(true);
    
    // vars to handle input
    const [username, setUsername] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    
    // var to show user a warning about registration status
    const [modalState, setModalState] = useState(false);
    
    // change spinner when form is ready
    useEffect(() => {
        setLoadingSpinner(false);
    });
    
    // give users input as they complete the form
    function handleChange(event) {
        
        // update state according to which field is being completed
        const inputId = event.target.id;
        const inputValue = event.target.value;
        if (inputId === 'username') {
            setUsername(inputValue);
        } else if (inputId === 'email') {
            setEmail(inputValue);
        } else if (inputId === 'password') {
            setPassword(inputValue);
        }
        
    }
    
    // do client side verification for form submition
    async function handleSubmit(event) {
        
        // user is informed login is being done
        setLoadingSpinner(true);
        
        // check if form is invalid
        // if it is not, do not submit the form
        // if it is valid, send the form
        const form = event.currentTarget;
        
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        } else {
            
            // prevent normal form submition
            event.preventDefault();
            
            // pack vars to send
            const params = {
                username: username,
                email: email,
                password: password
            }
            
            // send user data to api
            await submitUserData(formType, params)
            .then(result => {
                setModalState(result);
            });
            
            // hide loading spinner
            setLoadingSpinner(false);
            
        }
        
    }
    
    // a function to be passed to the modal component
    // allows a modal to be closed and redirected if needed
    function handleClose(event) {
        
        // hide modal
        setModalState({
            modalShow: false,
        });
        
        // redirect to correct place
        if (event.target.value === 'register200') {
            router.push('/login');
        }
        else if (event.target.value === 'login200') {
            router.push('/home');
        }
        
    }
    
    // render component
    return (
        
        <div className={`${styles.form} text-center`}>
        
            {
                
                loadingSpinner
                
                ? <LoadingSpinner />
                
                : <Form id="form" noValidate onSubmit={handleSubmit} validated={validated}>
                    
                    {
                        formType === 'register' &&
                            <Form.Group className={styles.formGroup}>
                                <Form.Label>Username</Form.Label>
                                <Form.Control className={styles.input} id="username" onChange={handleChange} required type="text" value={username}/>
                                <Form.Text>This is used to identify you across the platform.</Form.Text>
                                <Form.Text>It's shared with everyone and you can change it whenever you want, as long as it is unique.</Form.Text>
                            </Form.Group>
                    }
                    
                    <Form.Group className={styles.formGroup}>
                        <Form.Label>Email</Form.Label>
                        <Form.Control className={styles.input} id="email" onChange={handleChange} required type="email" value={email}/>
                        {formType === 'register' && <Form.Text>This is used to identify you in our database.</Form.Text>}
                        {formType === 'register' && <Form.Text>It is NOT shared with anyone and you can change it after registration</Form.Text>}
                    </Form.Group>
                    
                    <Form.Group className={styles.formGroup}>
                        <Form.Label>Password</Form.Label>
                        <Form.Control className={styles.input} id="password" onChange={handleChange} required type="password" value={password}/>
                        {formType === 'register' && <Form.Text>Password criteria:</Form.Text>}
                        {formType === 'register' && <Form.Text>This</Form.Text>}
                        {formType === 'register' && <Form.Text>And that</Form.Text>}
                    </Form.Group>
                    
                    <Button className={`${styles.button} shadow-none actionButton ripple`} type="submit" variant="primary">{capitalize(formType)}</Button>
                    
                    <SubmitModal modalState={modalState} handleClose={handleClose} />
                
                  </Form>
                
            }
        
        </div>
        
    );
    
}


// // // // //
// HELPER FUNCTIONS
// // // // //

// function to register/login user
async function submitUserData(action, params) {
    
    // form can be used to register or login
    if (action === 'register') {
        return await registerUser(action, params);
    } else if (action === 'login') {
        return await loginUser(action, params);
    }
    
}

// function to register user
async function registerUser(action, params) {
    
    // send data to api and receive response
    const response = await fetch('/api/register', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(params)
    });
    
    const data = await response.json();
    
    // redirect user to login page if registration was successful
    // otherwise, display an error and allow user to try again
    if (data.status === 200) {
        return {
            formType: 'Registration',
            message: 'Registration Successful!',
            buttonText: 'Continue',
            buttonValue: 'register200',
            modalShow: true
        };
    } else {
        return {
            formType: 'Registration',
            message: data.message,
            buttonText: 'Try Again',
            buttonValue: 'register500',
            status: data.status,
            modalShow: true
        };
    }
}

// login user using django
async function loginUser(action, params) {
    
    // send data to next api
    const response = await fetch('/api/login', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(params)
    });
    
    const data = await response.json();
    
    // if login was successful, save user refresh token in the browser for future reference
    // also stote in memory the normal token and logged in status
    // redirect user to home page if login was successful
    // otherwise, display an error and allow user to try again
    if (data.status === 200) {
        
        window.localStorage.setItem('treuzeRefreshToken', `${data.user}=${data.refresh}`);
        appContext.setContext.token(data.token);
        appContext.setContext.isUserLoggedIn(true);
        
        return {
            formType: 'Login',
            message: 'Login Successful!',
            buttonText: 'Continue',
            buttonValue: 'login200',
            modalShow: true
        };
        
    } else {
        return {
            formType: 'Login',
            message: data.message,
            buttonText: 'Try Again',
            buttonValue: 'login500',
            status: data.status,
            modalShow: true
        };
    }
    
}