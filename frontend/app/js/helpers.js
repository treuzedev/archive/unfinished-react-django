// // // // //
// GLOBAL FUNCTIONS
// // // // //

// parse local storage token
function parseRefreshToken(string) {
    const words = string.split('=');
    return {
        username: words[0],
        refresh: words[1]
    };
}

// check username input
function checkUsername(username) {
    
    // first check length
    // if length passes, check regex
    if (username.length <= 2) {
        return false;
    }
    
    // create a regex to check if there are only letters and numbers in the username
    // return result
    const regex = /^[a-z0-9]+$/i;
    return regex.test(username);
    
}

// check email input
function checkEmail(email) {
    
    // create a regex to check if email has correct input
    // return result 
    const regex = /^[^\s@]+@[^\s@]+[^\.\s]+$/;
    return regex.test(email);
    
}

// check password input
function checkPassword(password) {
    
    // first check length
    // if length passes, check regex
    if (password.length <= 7) {
        return false;
    }
    
    // create a regex to check if there are only letters and numbers in the username
    // needs to start with a letter
    // needs to have uppercase, lowercase and puctuation
    // return result
    const regex = /^[a-zA-Z](?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*\.])/;
    return regex.test(password);
    
}

// // // // //
// EXPORTS
// // // // //
export {
    checkEmail,
    checkPassword,
    checkUsername,
    parseRefreshToken,
};