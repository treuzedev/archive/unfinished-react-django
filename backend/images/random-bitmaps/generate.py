#####
# GENERATE RANDOM BITMAP IMAGES
#####

#####
# IMPORTS
#####

from numpy import random
import numpy
import matplotlib.pyplot as plt


#####
# PROGRAM
#####

# function to generate a random bitmap
def generateBitmap(fname='test.png', size=(8,8), cmap='hot'):
    bitmap = random.random(size)
    img = plt.imshow(bitmap, interpolation='nearest')
    img.set_cmap(cmap)
    plt.axis('off')
    plt.savefig(fname, bbox_inches='tight', pad_inches=0)
    plt.cla()
    plt.clf()
    plt.close('all')
    
# function to generate icons
def generateIcon(icon, fname='test.png', size=(8,8), cmap='hot'):
    iconFilter = random.random(icon.shape)
    newIcon = icon * iconFilter
    img = plt.imshow(newIcon, interpolation='nearest')
    img.set_cmap(cmap)
    plt.axis('off')
    plt.savefig(fname, bbox_inches='tight', pad_inches=0)
    plt.cla()
    plt.clf()
    plt.close('all')
    
    
# every possible type of cmap
cmaps = ['viridis', 'plasma', 'inferno', 'magma', 'cividis',
        'Greys', 'Purples', 'Blues', 'Greens', 'Oranges', 'Reds',
        'YlOrBr', 'YlOrRd', 'OrRd', 'PuRd', 'RdPu', 'BuPu',
        'GnBu', 'PuBu', 'YlGnBu', 'PuBuGn', 'BuGn', 'YlGn',
        'binary', 'gist_yarg', 'gist_gray', 'gray', 'bone', 'pink',
        'spring', 'summer', 'autumn', 'winter', 'cool', 'Wistia',
        'hot', 'afmhot', 'gist_heat', 'copper',
        'PiYG', 'PRGn', 'BrBG', 'PuOr', 'RdGy', 'RdBu',
        'RdYlBu', 'RdYlGn', 'Spectral', 'coolwarm', 'bwr', 'seismic',
        'twilight', 'twilight_shifted', 'hsv',
        'Pastel1', 'Pastel2', 'Paired', 'Accent',
        'Dark2', 'Set1', 'Set2', 'Set3',
        'tab10', 'tab20', 'tab20b', 'tab20c',
        'flag', 'prism', 'ocean', 'gist_earth', 'terrain', 'gist_stern',
        'gnuplot', 'gnuplot2', 'CMRmap', 'cubehelix', 'brg',
        'gist_rainbow', 'rainbow', 'jet', 'turbo', 'nipy_spectral', 'gist_ncar']
        
print(f'number of cmaps: {len(cmaps)}')
        
# possible sizes
sizes = [(4,4), (8,8), (16,16), (32,32)]

# some possible images
                
face = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0],
        [0, 0, 0, 1, 1, 2, 2, 2, 1, 1, 1, 1, 0, 0, 0, 0],
        [0, 0, 1, 1, 1, 2, 2, 2, 1, 1, 1, 1, 1, 0, 0, 0],
        [0, 1, 1, 1, 1, 2, 2, 2, 1, 1, 1, 1, 1, 1, 0, 0],
        [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
        [0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
        [0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
        [0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
        [0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
        [0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
        [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0],
        [0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0],
        [0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0],
        [0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]
                
sword = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0],
         [0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0],
         [0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]
    
robot = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0],
         [0, 0, 0, 1, 2, 2, 2, 2, 1, 0, 0, 0],
         [0, 0, 0, 0, 1, 2, 2, 1, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 2, 2, 0, 0, 0, 0, 0],
         [0, 0, 1, 1, 1, 2, 2, 1, 1, 1, 0, 0],
         [0, 0, 0, 1, 1, 2, 2, 1, 1, 0, 0, 0],
         [0, 0, 0, 0, 0, 2, 2, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 2, 2, 0, 0, 0, 0, 0],
         [0, 0, 0, 1, 2, 2, 2, 2, 1, 0, 0, 0],
         [0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]

space = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0],
         [0, 0, 0, 0, 1, -1, -1, 1, 0, 0, 0, 0],
         [0, 0, 0, 1, 1, -1, -1, 1, 1, 0, 0, 0],
         [0, 0, 0, 1, 1, -1, -1, 1, 1, 0, 0, 0],
         [0, 0, 1, 1, 1, -1, -1, 1, 1, 1, 0, 0],
         [0, 1, 1, 1, 2, 2, 2, 2, 1, 1, 1, 0],
         [0, 1, 1, 1, 2, 2, 2, 2, 1, 1, 1, 0],
         [0, 1, 1, 1, 2, 2, 2, 2, 1, 1, 1, 0],
         [0, 1, 1, 1, 1, -1, -1, 1, 1, 1, 1, 0],
         [0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]

# transform to numpy array
face = numpy.array(face)
sword = numpy.array(sword)
robot = numpy.array(robot)
space = numpy.array(space)
            
# test            
generateBitmap(fname=f'bitmaps/{cmaps[62]}-{sizes[3]}-{0}.png', size=sizes[3], cmap=cmaps[62])
generateIcon(face, fname=f'icons/face-{cmaps[7]}-{0}.png', cmap=cmaps[7])
generateIcon(sword, fname=f'icons/sword-{cmaps[35]}-{0}.png', cmap=cmaps[35])
generateIcon(space, fname=f'icons/space-{cmaps[41]}-{0}.png', cmap=cmaps[41])
generateIcon(robot, fname=f'icons/robot-{cmaps[50]}-{0}robot.png', cmap=cmaps[50])

# generate x bitmaps of each type and size and x icons of each type
# for i in range(0, 1):
#     print(f'iteration: {i}')
#     for cmap in cmaps:
#         print(f'generating cmap: {cmap}')
#         generateIcon(face, fname=f'icons/face-{cmap}-{i}.png', cmap=cmap)
#         generateIcon(sword, fname=f'icons/sword-{cmap}-{i}.png', cmap=cmap)
#         generateIcon(space, fname=f'icons/space-{cmap}-{i}.png', cmap=cmap)
#         generateIcon(robot, fname=f'icons/robot-{cmap}-{i}robot.png', cmap=cmap)
#         for size in sizes:
#             print(f'generating size: {size}')
#             generateBitmap(fname=f'bitmaps/{cmap}-{size}-{i}.png', size=size, cmap=cmap)