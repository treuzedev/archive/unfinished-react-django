#####
# IMPORTS
#####

# django imports
from django.http import JsonResponse

# aws imports
import boto3
import botocore
from boto3.dynamodb.conditions import Attr

# system imports
import os


#####
# GET SAMPLE QUESTIONS HELPER FUNCTIONS
#####

# get sample questions from dynamodb
def getSampleQuestions():
    
    # connect to dynamodb
    awsRegion = os.environ.get('AWS_REGION')
    tableName = os.environ.get('SAMPLE_QUESTIONS_TABLE')
    db = boto3.resource('dynamodb', region_name=awsRegion)
    table = db.Table(tableName)
    
    # retrieve all sample questions
    # if an error occurs, send back that error
    try:
        response = table.scan(
            FilterExpression=Attr('type').eq('sampleQuestion')
        )
    except botocore.exceptions.ClientError as error:
        return JsonResponse({
                'message': error.response['Error']['Message'],
                'status': error.response['ResponseMetadata']['HTTPStatusCode']
            })
    
    # return questions
    return JsonResponse({
            'message': 'all okay',
            'questions': response['Items'],
            'status': 200
        })