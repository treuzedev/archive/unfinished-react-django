#####
# IMPORTS
#####

# django imports
from django.http import JsonResponse

# aws imports
import boto3
import botocore
from boto3.dynamodb.conditions import Attr

# system imports
import json
import requests
import time
import os
import base64
import re

# other imports
from api.helpers.helpers import getDynamoTable


#####
# GET USER INFO
#####

# login user
def getUserInfo(user):
    
    # connect to dynamodb
    usernamesTable = getDynamoTable('USERNAMES_TABLE')
    emailsTable = getDynamoTable('EMAILS_TABLE')
    
    # get user info from dynamo
    # if an error occurs, send back that error
    userInfo = {}
    try:
        
        # username (where an email is found)
        response = usernamesTable.get_item(
            Key={
                'username': user,
            },
        )
        
        userInfo['username'] = user
        
        # email and password
        response = emailsTable.get_item(
            Key={
                'email': response['Item']['email'],
            },
        )
        
        userInfo['email'] = response['Item']['email']
        userInfo['password'] = response['Item']['password']
        
        # profile picture
        userInfo['photo'] = getProfilePhoto(user)
        
    except botocore.exceptions.ClientError as error:
        return JsonResponse({
                'message': error.response['Error']['Message'],
                'status': error.response['ResponseMetadata']['HTTPStatusCode']
            })
        
    # return user token
    return JsonResponse({
        'message': 'all is well',
        'status': 200,
        'userInfo': userInfo,
    })
    
# get profile photo link from dynamo and photo from s3
# transform it to a base64 enconded string
def getProfilePhoto(user):
    
    # connect to dynamo
    table = getDynamoTable('USERS_TABLE')
    
    # get current user profile link photo
    # if an error occurs, send back that error
    try:
        response = table.get_item(
            Key={
                'username': user
            },
            ProjectionExpression='profilePictureKey'
        )
    except botocore.exceptions.ClientError as error:
        return JsonResponse({
                'message': error.response['Error']['Message'],
                'status': error.response['ResponseMetadata']['HTTPStatusCode']
            })
    
    # if no item was return, user still hasn't provided a profle picture
    # if he has, get it from s3
    if response['Item']:
        
        # get file type
        fileExtension = re.search("\.[a-z]*$", response['Item']['profilePictureKey']).group(0)
        
        # connect to s3
        TREUZE_BUCKET = os.environ.get('TREUZE_BUCKET')
        s3 = boto3.client('s3')
        
        # to to get object
        # return error otherwise
        try:
            response = s3.get_object(
                Bucket=TREUZE_BUCKET,
                Key=response['Item']['profilePictureKey']
            )
        except botocore.exceptions.ClientError as error:
            return JsonResponse({
                    'message': error.response['Error']['Message'],
                    'status': error.response['ResponseMetadata']['HTTPStatusCode']
                })
        
        # return photo as base 64 encoded bytes string
        return {
            'status': 200,
            'message': 'Photo found! Decoded bytes to string and encoded string using base64.',
            'data': base64.b64encode(response['Body'].read()).decode('UTF-8'),
            'fileExtension': fileExtension,
        }
        
    else:
        return {
            'status': 404,
            'message': 'user has no photo',
        }
        