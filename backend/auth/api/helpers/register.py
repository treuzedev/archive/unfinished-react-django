#####
# IMPORTS
#####

# django imports
from django.http import JsonResponse
from django.db import IntegrityError

# # custom user model
from api.models import User

# aws imports
import boto3
import botocore
from boto3.dynamodb.conditions import Attr

# system imports
import os
import json
import time

# other imports
from api.helpers.helpers import getDynamoTable


#####
# REGISTER
#####

def registerUser(request):
    
    # get user parameters to register
    params = json.loads(request.body)
        
    # connect to dynamodb
    usersTable = getDynamoTable('USERS_TABLE')
    emailsTable = getDynamoTable('EMAILS_TABLE')
    usernamesTable = getDynamoTable('USERNAMES_TABLE')
    
    # additional info
    timestamp = str(int(time.time()))
            
    # try to create an username entry
    # if there is an error, send it back
    # username is registered first as it would be more common and more prone to failures
    try:
        response = usernamesTable.put_item(
            Item={
                'username': params['username'],
                'email': params['email'],
            },
            ConditionExpression='NOT contains(username, :username)',
            ExpressionAttributeValues={
                ':username': params['username'],
            }
        )
    except botocore.exceptions.ClientError as error:
        return JsonResponse({
                'message': 'Error! Username is not unique. Please try again.',
                'status': error.response['ResponseMetadata']['HTTPStatusCode']
            })
    
    # try to create an email entry
    # if there is an error, send it back
    # if there is an error, delete previously created entry in username table
    try:
        response = emailsTable.put_item(
            Item={
                'password': params['password'],
                'email': params['email'],
            },
            ConditionExpression='NOT contains(email, :email)',
            ExpressionAttributeValues={
                ':email': params['email'],
            }
        )
    except botocore.exceptions.ClientError as error:
        response = usernamesTable.delete_item(
            Key={
                'username': params['username'],
            }
        )
        return JsonResponse({
                'message': 'Error! Email not unique. Please try again.',
                'status': error.response['ResponseMetadata']['HTTPStatusCode']
            })
    
    # try to create an user stats
    # if there is an error, send it back
    try:
        response = usersTable.put_item(
            Item={
                'username': params['username'],
                'createdAt': timestamp,
                'questionsRetrieved': 0,
                'correctQuestions': 0,
                'successRatio': 0
            },
        )
    except botocore.exceptions.ClientError as error:
        return JsonResponse({
                'message': error.response['Error']['Message'],
                'status': error.response['ResponseMetadata']['HTTPStatusCode']
            })
    
    # return a response saying user is registered
    return JsonResponse({
        'message': 'user is registered',
        'status': 200
    })
    