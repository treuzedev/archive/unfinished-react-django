#####
# IMPORTS
#####

# django imports
from django.http import JsonResponse
from django.contrib.auth import authenticate, login
from django.db import IntegrityError
from django.core import serializers

# aws imports
import boto3
import botocore
from boto3.dynamodb.conditions import Attr

# system imports
import json
import requests

# other imports
from api.helpers.helpers import getDynamoTable

# custom user model
from api.models import User



#####
# LOGIN HELPER FUNCTIONS
#####

# login user
def loginUser(request):
    
    # get user parameters to login
    params = json.loads(request.body)
    
    # connect to dynamodb
    emailsTable = getDynamoTable('EMAILS_TABLE')
    usernamesTable = getDynamoTable('USERNAMES_TABLE')
    
    # check if user is registered
    # return an error otherwise
    try:
        response = emailsTable.query(
            KeyConditionExpression='email = :email',
            ExpressionAttributeValues={
                ':email': params['email'],
            }
        )
    except botocore.exceptions.ClientError as error:
        return JsonResponse({
                'message': 'Error! User is not registered..',
                'status': error.response['ResponseMetadata']['HTTPStatusCode']
        })
        
    # check if there is an user or if more than one user came back
    # if so, return error
    if len(response['Items']) > 1:
        return JsonResponse({
            'message': 'Error! More than one user with that email. Please contact support.',
            'status': 403,
        })
    elif len(response['Items']) < 1:
        return JsonResponse({
            'message': 'Error! Email not found.',
            'status': 403,
        })
    else:
        pass
        
            
    # check if password is correct
    # return with error code otherwise
    if params['password'] != response['Items'][0]['password']:
        return JsonResponse({
            'message': 'Error! Password is not correct.',
            'status': 403,
        })
        
    # try to get user username
    # return an error otherwise
    try:
        response = usernamesTable.scan(
            FilterExpression='email = :email',
            ProjectionExpression='username',
            ExpressionAttributeValues={
                ':email': params['email'],
            }
        )
    except botocore.exceptions.ClientError as error:
        return JsonResponse({
                'message': 'Error! User is not registered..',
                'status': error.response['ResponseMetadata']['HTTPStatusCode']
        })
    
    # create new token for further authentication
    # ensure only one username came back
    if len(response['Items']) == 1:
        
        # try to authenticate the user
        user = authenticate(request, username=response['Items'][0]['username'], password=params['password'])
        
        # login user if one was returned
        # if not, try to register it in local database
        # if that fails, return an error
        if user is not None:
            login(request, user)
        else:
            
            try:
                user = User.objects.create_user(response['Items'][0]['username'], params['email'], params['password'])
                user.save()
            except IntegrityError as e:
                return JsonResponse({
                    'message': f'{e.__cause__}',
                    'status': 512
                })
                
            user = authenticate(request, username=response['Items'][0]['username'], password=params['password'])

        params = {
            'username': response['Items'][0]['username'],
            'password': params['password'],
        }
        
        r = requests.post('http://localhost:5000/token/create', data=params).json()
            
    else:
        return JsonResponse({
            'message': 'Something is wrong in the backend; please contact support.',
            'status': 503,
        })
        
    # return user token
    return JsonResponse({
        'message': 'user is logged in',
        'status': 200,
        'user': response['Items'][0]['username'],
        'token': r['access'],
        'refresh': r['refresh'],
    })
    