#####
# IMPORTS
#####

# aws imports
import boto3
import botocore
from boto3.dynamodb.conditions import Attr

# system imports
import os

# get a connection to a dynamodb table
def getDynamoTable(table):
    awsRegion = os.environ.get('AWS_REGION')
    tableName = os.environ.get(table)
    db = boto3.resource('dynamodb', region_name=awsRegion)
    return db.Table(tableName)
    

# get a connection to a dynamodb db
def getDynamoClient():
    return boto3.client('dynamodb')