#####
# IMPORTS
#####

# django imports
from django.db import models
from django.contrib.auth.models import AbstractUser


#####
# MODELS
#####

# a custom user model to allow token authentication
class User(AbstractUser):
    pass
