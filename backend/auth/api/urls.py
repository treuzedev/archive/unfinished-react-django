#####
# IMPORTS
#####

# django imports
from django.urls import path

# jwt imports
# from rest_framework.authtoken import views as v
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

# views imports
from . import views

urlpatterns = [
    
    path('register', views.register, name='register'),
    path('login', views.login, name='login'),
    path('u', views.userInfo, name='userInfo'),
    path('u/<str:u>/change-profile-info', views.profileInfo, name='changeProfileField'),
    path('u/upload-photo', views.uploadPhoto, name='uploadPhoto'),
    path('token/create', TokenObtainPairView.as_view(), name='token'),
    path('token/refresh', TokenRefreshView.as_view(), name='tokenRefresh'),
    path('token/check', views.checkToken, name='checkToken'),
    
]