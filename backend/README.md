# backend

### variables needed by djangp
export CLOUD_IP=$(curl -s http://169.254.169.254/latest/meta-data/public-ipv4)
export DJANGO_IP=$(curl -s http://169.254.169.254/latest/meta-data/local-ipv4)
export FRONTEND_IP="10.0.0.12"
export AWS_REGION="eu-west-1"
export SAMPLE_QUESTIONS_TABLE="sampleQuestions"
export QUESTIONS_TABLE="test"
export AWS_SECRET_ACCESS_KEY=
export AWS_ACCESS_KEY=
