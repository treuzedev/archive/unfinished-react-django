#####
# CONFIG
#####

terraform {

  required_version = "~> 0.14.7"

  required_providers {

    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.29.0"
    }

  }

}


#####
# PROVIDER
#####
provider "aws" {
  region = "eu-west-1"
}



#####
# RESOURCES
#####

##### create s3 bucket
resource "aws_s3_bucket" "s3_bucket" {

  bucket = "treuze"

  lifecycle {
    prevent_destroy = true
  }

  versioning {
    enabled = true
  }

  server_side_encryption_configuration {

    rule {

      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }

    }

  }

}

