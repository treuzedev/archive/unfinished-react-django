#!/bin/bash

# remote state bucket needs to exist before using remote state for other applications
# ie, needs to be created manually
# terraform apply -chdir=remote-state